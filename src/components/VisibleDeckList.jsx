import {connect} from 'react-redux'
import DeckList from './DeckList'
//import {ShowDecks} from '../actions'

const getVisibleDecks = (decks) => {
    return decks
}

const mapStateToProps = state => ({
    decks: getVisibleDecks(state.decks, state.visibilityFilter)
})

export default connect(
    mapStateToProps
)(DeckList)
