import React from 'react'
import '../style/deck.css'
import { connect } from 'react-redux'
import { Formik, Field, FieldArray, Form } from 'formik'

import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import '../style/deck.css'
import '../style/main.css'
import testPhase from '../utils/testPhase';

// TODO: Funktioniert nicht.
/*
const InputSchema = Yup.object().shape({
  word: Yup.string()
    .max(50, 'Zu lang!')
    .required('Das Wort darf nicht leer sein!'),
  definition: Yup.string()
    .max(50, 'Zu lang!')
    .required('Die Definition darf nicht leer sein!')
});
*/


const TableInput = (props) => {
  let rowIndex = 0

  return (
        <div>
            <Formik
            initialValues={props.initialCards}
            onSubmit={(values) => {
              if(values.cards){
                props.onSubmit(values.cards);
              }    
            }}
            >

            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting
            }) => (
                <Form>
                    <FieldArray
                      name="cards"
                      render={arrayHelpers => (
                        <div>
                          {values.cards.map((cards, index) => (
                            values.cards.length === 1 
                            ?
                            <div key={index} className="card-wrapper">
                              <div id='cont-left'><Field id="card-field" placeholder="Wort" name={`cards.[${index}].word`} /></div>
                              <div id='cont-right'><Field id="card-field" placeholder="Definition" name={`cards[${index}].definition`} /></div>
                            </div>
                            :
                            <div key={index} className="card-wrapper">
                              <div id='cont-left'><Field id="card-field" placeholder="Wort" name={`cards[${index}].word`} /></div>
                              <div id='cont-right'><Field id="card-field" placeholder="Definition" name={`cards[${index}].definition`} />
                              <button id="wrap-button" type="button" onClick={() => arrayHelpers.remove(index)}>
                              <FontAwesomeIcon icon={faTrash}/>
                              </button>
                              </div>
                            </div>
                          ))}
                          <button
                            type="button"
                            id="create-button"
                            onClick={() => arrayHelpers.push({ word: '', definition: '', rowIndex: rowIndex++, testPhase: testPhase.ph1, nextDate: 0})}>
                            Neue Karte hinzufügen                             
                          </button>
                          <button id="submit-button" type="submit">{props.createDeck ? 'Deck erstellen' : 'Deck speichern'}</button>
                        </div>
                        )}
                        />
                </Form>
            )}
            </Formik>
        </div>
    );
};

export default connect()(TableInput)
