import React from 'react'

import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const DeckEditOptions = (title, description, props) => (

    <form id="edit-enclosing"
    onSubmit={e => { 
        e.preventDefault()
        }}>
        <input className="deck-name edit-mode" type="text" defaultValue={title} placeholder="Gib einen Titel ein" ref={node => (title = node)} />
        <input className="deck-desc edit-mode" type="text" defaultValue={description} placeholder="Beschreibung hinzufügen" ref={node => (description = node)}/>
        <button type="submit">Speichern</button>
    </form>
)


export default DeckEditOptions;
