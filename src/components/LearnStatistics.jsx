import React from "react";
import { Tooltip, ChartProvider, ArcSeries, Arc } from 'rough-charts'
import { colors } from '../utils/colors'
  
  const LearnStatistics = props => ( 
    <ChartProvider
      height={400}
      data={props.dataValues}
    >
      <ArcSeries
    dataKey="cards"
  >
    {
      (item, itemProps, index) => (
        <Arc
          {...itemProps}
          key={index}
          options={{
            fill: colors[index % colors.length],
          }}
          outerRadius={
            itemProps.outerRadius * 0.3
            + (index / props.dataValues.length)
            * itemProps.outerRadius * 0.7
          }
        />
      )
    }
  </ArcSeries>
        <Tooltip>{({name, cards}) => `${name}: ${cards} Karte(n)`}</Tooltip>
    </ChartProvider>
  )

  export default LearnStatistics;