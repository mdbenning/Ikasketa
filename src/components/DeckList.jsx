import React from 'react';
import PropTypes from 'prop-types'
import Deck from './Deck'

var noDecks = String.raw`
 _____
/    /|_ ______________________________________________
/    // /|                                            /|
(====|/ //   An apple a day...            _QP_       / |
(=====|/     keeps the teacher at bay    (  ' )     / .|
(====|/                                   \__/     / /||
/_________________________________________________/ / ||
|  _____________________________________________  ||  ||
| ||                                            | ||
| ||                                            | ||
| |                                             | |
`

const DeckList = ({ decks }) => (
        decks.length === 0 || decks === undefined ?
            <div><h2>Du hast bis jetzt keine Decks erstellt.</h2><pre>{noDecks}</pre></div>
        :
        <ul>
            {decks.map((deck, rowID) => (
                <Deck key={rowID+1} { ...deck } />
            ))
           }
        </ul>
        
)

DeckList.propTypes = {
    decks: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired
    ).isRequired
}

export default DeckList;
