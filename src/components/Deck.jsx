import React from 'react';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import './cards.svg'
import { faStickyNote } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Deck = ({name, description, id}) => (
            <Link to={`/deck/${id}`}>
                <div id="unit-wrapper">
                    <div className="unit-image">
                        <FontAwesomeIcon id="deck-item" size="4x" icon={faStickyNote}/>
                    </div>
                    <div className="unit-container">
                        <h2 id="unitTitle">
                            {name}
                        </h2>
                        <h3 style={{fontStyle: 'italic'}}>{description}</h3>
                    </div>
                </div>
            </Link>
      )


Deck.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
}

export default Deck;
