import React from 'react';
import '../style/main.css';

//var __help_de = JSON.parse('../contents/help.js')

const NotFound = () => {

    var nf = String.raw`
                    .::.
                  .:'  .:
        ,MMM8&&&.:'   .:'
       MMMMM88&&&&  .:'
      MMMMM88&&&&&&:'
      MMMMM88&&&&&&
    .:MMMMM88&&&&&&
  .:'  MMMMM88&&&&
.:'   .:'MMM8&&&'
:'  .:'
'::'`;

    return(
        <div className="component-view">
            <h1>Leer wie im Kosmos ...</h1>
            <h2>404</h2>
            <pre>
                {nf}
            </pre>
        </div>
    )
}

export default NotFound;