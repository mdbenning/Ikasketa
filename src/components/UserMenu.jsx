import React, { useState, useEffect } from 'react';
import { faUser, faCog, faQuestionCircle, faSignOutAlt, faAddressCard } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import auth from '../utils/auth';
import history from '../utils/history'

/*function mapStateToProps(){
    //Falls isAuthenticated im store auf false gesetzt wird, wird das UserMenu nicht angezeigt.
}*/

const UserMenu = () => {
    const [isAuthenticated, setAuthenticated] = useState(false)

    useEffect(() => {
        if(auth.isAuthenticated()){
            setAuthenticated(true)
        }
    }, [])

    const [isShown, setIsShown] = useState(false)
    if(isAuthenticated){
    return(
        <div id='user-menu' onClick={() => setIsShown(true)} onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)} >
            <div className='user-menu-icon' ><FontAwesomeIcon icon={faUser}/> Name</div>
            {isShown ?
            <div className='user-menu-content'>
                <ul className='user-menu-list'>
                    <li><FontAwesomeIcon icon={faAddressCard}/> Profil</li>
                    <li><FontAwesomeIcon icon={faQuestionCircle}/> Statistik</li>
                    <li><FontAwesomeIcon icon={faCog}/> Einstellungen</li>
                    <li onClick={() => {auth.logout(() => {history.push('/')})}}><FontAwesomeIcon icon={faSignOutAlt}/> Abmelden</li>
                </ul>
            </div> : ''}
        </div>
    )}
    else{
        return(
            <div><button>Anmelden</button> <span>oder</span> <button>Registrieren</button></div>
        )
    }
}

export default UserMenu;