import React from 'react'

import VisibleDeckList from "../components/VisibleDeckList";
import Statistics from '../components/Statistics';

function Main() {
    return (
        <div className="component-view">
            <h1 id="title">Willkommen!</h1>
            <Statistics />
            <VisibleDeckList />
        </div>
      );
  }
  
  export default Main;
  