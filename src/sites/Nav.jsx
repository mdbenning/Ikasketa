import React from 'react';
import '../style/main.css'
import {NavLink} from 'react-router-dom';
// get our fontawesome imports
import { faHome, faHandsHelping, faFolderPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
//import UserMenu from '../components/UserMenu';
//import mainLogo from '../assets/ika_logo.svg'

function Nav(){
    return(
        <nav className="main-nav">
            <NavLink to="/" exact><h2 id="app-title">ika</h2></NavLink>
            <ul className="links-nav">
                <NavLink to='/' exact activeClassName='is-active'><li><FontAwesomeIcon icon={faHome}/> Alle Decks</li></NavLink>
                <NavLink to='/help' activeClassName='is-active'><li><FontAwesomeIcon icon={faHandsHelping}/> Hilfe</li></NavLink>
                <NavLink to='/create' activeClassName='is-active add-deck' className='add-deck'><li><FontAwesomeIcon icon={faFolderPlus}/> Neues Deck</li></NavLink>
                <NavLink to='/create' activeClassName='is-active'><li><FontAwesomeIcon class="add-deck-mobile" size='1x' icon={faFolderPlus}/></li></NavLink>
                {/*<UserMenu/>*/}
            </ul>
        </nav>
    )
}

export default Nav;