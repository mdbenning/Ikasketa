import React from 'react';
import '../style/guide.css';

function Help() {
    return(
        <div className="component-view">
            <h1 id="title">Hilfe</h1>
            <h2><i>Was</i> ist Ikasketa?</h2>
            <p>Ikasketa ist ein freier Online/Offline-Vokabeltrainer ohne Premiumoptionen und übertriebenen Spielereien.
            Es gibt Dir die Möglichkeit, dich auf das zu konzentrieren, was wichtig ist: lernen. Und lernen ist ein Gut,
            was jedem zugute kommen sollte.<br/>
            Ikasketa bietet dir verschiedene Möglichkeiten zu lernen. Du kannst für dich ganz persönlich Vokabelkarten erstellen,
            um z. B. deine Sprachkenntnisse auszuweiten, oder auch um Begrifflichkeiten anderer Themenbereiche zu studieren.
            </p>
            <h2>Bedienungsweise</h2>
            Ikasketa ist einfach zu handhaben.
            <h3>Erstellen von Decks</h3>
            <p>Über den Reiter "Neues Deck" kannst du ein neues Kartendeck erstellen. In diesem kannst du Namen, Beschreibung und natürlich Karten erstellen.
                Wenn alle Einträge getätigt sind, kannst du das Deck erstellen. Du wirst auf die Hauptseite geleitet und siehst dein neues Deck in der Liste.
            </p>
            <h3>Decks</h3>
            <p>Du kannst die neu erstellen Decks in der Liste anklicken. Dir wird eine Übersicht über Name, Beschreibung und Karten gegeben. So hast du hier die Möglichkeit,
                mit einem Klick auf den Stift das Deck zu bearbeiten. Wenn deine Änderungen fertig sind, kannst du diese abspeichern.
                Ebenfalls kannst du das Deck löschen, falls du es nicht mehr benötigst bzw. damit unzufrieden bist.
            </p>
            <h3>Lernmodus</h3>
            <p>Im Lernmodus kannst du die Karten des jeweiligen Decks in gegebener Weise durchgehen. Im Modus kannst du jeweilige Fragen sehen und anschließend 
                aufdecken. Je nach eigenem Gefühl kannst du angeben, dass du dich sicher mit dieser Karte sicher fühlst, oder ob du sie nochmal wiederholen möchtest.
                Der Lernmodus ist jederzeit abrufbar und bewertet deine Leistungen nicht.
            </p>
            <i>Tipp: Wenn du bestimmte Karten zum aller ersten Mal siehst, empfiehlt es sich, den Lernmodus zu verwenden, bevor du den Testmodus benutzt.</i>
            <h3>Testmodus</h3>
            <p>Im Gegensatz zum Testmodus werden die einzelnen Karten gezielt abgefragt. So werden hier auch Fragen gestellt, nur musst du in diesem Fall die richtige
                Antwort in ein Textfeld eintippen. Keine Sorge, Tippfehler werden nicht als wirkliche Fehler gezählt.
                Die Karten in diesem Deck werden verschiedenen <b>Phasen</b> zugeordnet. Falls du eine richtige Antwort auf eine Karte gibst, wird die jeweilige Karte in
                eine höhere Phase verschoben. Wenn du eine falsche Antwort gibst, bleibt sie in der gleichen Phase. Je höher die Phase ist, desto länger sind die Abstände bis zum Abfragen.
                Wenn du mit deiner Lernsession durch bist, dauert es mindestens einen Tag, bis die Karten erneut abgefragt werden. Wenn du aber trotzdem nochmal den Lernmodus
                verwenden möchtest, kannst du das über die Option "Trotzdem lernen" machen. Das hat jedoch keine Wirkung auf die Phase, in welcher die Karte ist.
            </p>
            <h2>Kontakt</h2>
            <p>Wenn du Verbesserungsvorschläge hast oder einen Bug gefunden hast, dann schreibe mir doch bitte über meine Mailadresse: michael.benning[ ä t]]pm.me.
                Ach ja, ich arbeite übrigens auch an anderen Projekten, wie z.B. an einer gleichnamigen Sprachlernplattform. Mehr Informationen findest du auf <a id="link" href={"https://git.mdbenning.de"}>git.mdbenning.de</a>!
            </p>
        </div>
    )
}

export default Help;