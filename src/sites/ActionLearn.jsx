import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import learnDif from '../utils/difficulties'
import '../style/test.css'
import NotFound from '../components/NotFound';

function mapStateToProps(state, ownProps) {
    const { id } = ownProps.match.params
    const cards = state.cideck.find(cards => cards.id === id);
    return {cards, id};
}

const ActionLearn = (props) => {

    // Hook declaration
    const [isVisible, setIsVisible] = useState(false);
    const [isDone, setIsDone] = useState(false)
    const [currentCard, setNextCard] = useState(0)
    const [remaining, setRemaining] = useState(0);
    const [queueVar, setQueueVar] = useState(1)
    const [queue, setQueue] = useState([]) 

    let learnQueue = queue

   useEffect(() => {
        if(props.cards !== undefined){
            setNextCard(props.cards.cards[0])
            setRemaining(props.cards.cards.length)
        }
    }, [props.cards])

    function doReset(){
        setIsVisible(false)
        setIsDone(false)
        setRemaining(props.cards.cards.length)
        setNextCard(props.cards.cards[0])
        setQueueVar(1)
    }

    if(props.cards === undefined) {
        return(
            <NotFound />
        )
    }
    else {
        const cards = props.cards.cards;
        function selectCard(){
            // Hier soll state für das jeweilige Lernlevel verändert werden und
            // dadurch bestimmt werden, welche Karte als nächstes drankommt.
            if(queueVar < cards.length){
                setQueueVar(queueVar+1)
                return(setNextCard(cards[queueVar]))
            }
            else if(queueVar === cards.length && learnQueue.length != null){
                let next = learnQueue.shift()
                return(setNextCard(next))
            }
            if (typeof learnQueue != "undefined" && learnQueue != null && learnQueue.length != null && learnQueue.length > 0 && remaining === 0) {
                // Umständlich, aber: Wenn array existiert und nicht leer ist...
                setIsDone(true);
                return null;
            }
        }

        const onEvent = (dif) => {
            if(dif === learnDif.pass){
                setRemaining(remaining-1)
            } else if(dif === learnDif.again){
                learnQueue.push(currentCard)
                setQueue(learnQueue)
            }
            selectCard()
            setIsVisible(false)
        }
        
            return(
                isDone || currentCard === undefined ?
                <div className="component-view">
                    <h1>Fertig!</h1>
                    <p>Wenn du möchtest, kannst du die Karten nochmals lernen. Ansonsten hast du die Möglichkeit, den Schreib- oder Testmodus zu benutzen.</p>
                    <Link to={`/deck/${props.id}`}><button>Zurück zur Übersicht</button></Link>
                    <button onClick={doReset}>Wiederholen</button>
                </div>
                :
                <div className="component-view">
                    <i>verbleibende Karten: {remaining}</i>
                    <div className="test-card"> 
                        <div className="card-top">
                        <p>{currentCard.word}</p>
                        </div>
                        <div onClick={() => setIsVisible(true)} className="card-bottom">
                            {isVisible ? 
                                <div className="spoiler">
                                <p id="revealed">{currentCard.definition}</p>
                                </div>
                            :
                            <i>Zum Aufdecken hier klicken</i>
                            }
                        </div>
                       
                    </div>
                        {isVisible ? 
                        <div className="button-group"> 
                            <button onClick={() => onEvent(learnDif.again)} className="difficulty-button again">Nochmal</button>
                            <button onClick={() => onEvent(learnDif.pass)} className="difficulty-button easy">Okay</button></div>
                            : 
                            ''}
                    </div>
                )
    }
}

export default connect(mapStateToProps)(ActionLearn);