import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import compare from 'fuzzy-comparison';

import NotFound from '../components/NotFound'

import '../style/test.css'
import '../style/loader.css'

import testPhase from '../utils/testPhase';
import dateSelect from '../utils/dateSelect'
import { setNextDate, incrementPhase, decrementPhase } from '../actions';
import LearnStatistics from '../components/LearnStatistics';

const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps.match.params
    const cards = state.cideck.find(cards => cards.id === id);
    return {cards, id};
}

const mapDispatchToProps = (dispatch) => {
    return {
        setNextDate: (rowIndex, nextDate, id) => dispatch(setNextDate(rowIndex, nextDate, id)),
        incrementPhase: (rowIndex, currentPhase, id) => dispatch(incrementPhase(rowIndex, currentPhase, id)),
        decrementPhase: (rowIndex, currentPhase, id) => dispatch(decrementPhase(rowIndex, currentPhase, id))
    }
}

const ActionLearn = (props) => {
    let answer
    var date = new Date()

    const [isVisible, setIsVisible] = useState(false);
    const [isCorrect, setResult] = useState(false);
    const [isDoneLoading, setIsDoneLoading] = useState(false)
    const [queue, setQueue] = useState([])
    const [wrongQueue] = useState([])
    const [currentCard, setNextCard] = useState(0)
    const [isDone, setIsDone] = useState(false)
    const [dispatchMode, setDispatchMode] = useState(true)

    let learnQueue = queue
    let answeredWrong = wrongQueue

    useEffect(() => {
        if(props.cards !== undefined){
            let cards = props.cards.cards;
            // Hier sollen alle zu lernenden Karten ausgewählt werden. 
            // Das soll entweder nach der Kondition 'noch nie gelernt'  
            // oder nach dem jeweiligen Datum abgefragt werden.
            //for(iterator; iterator < props.cards.length; setIterator(iterator+1)){
            // Die Angaben werden nach den jeweiligen Phasen definiert. 
            // Bestimmte Karten werden erst nach mehreren Tagen abgefragt, also sich 
            // in einer anderen Phase befindet.
            for(let i = 0; i < cards.length; i++){
                if(cards[i].nextDate === 0){
                    learnQueue.push(cards[i])
                } else if (cards[i].nextDate <= date){
                    learnQueue.push(cards[i])
                } else if(!dispatchMode){
                    learnQueue.push(cards[i])
                }
            }
            // Wenn es keine Karten zu lernen gibt, soll trotzdem die
            // Möglichkeit gegeben werden, die Karten nochmals zu testen. Das
            // gibt hauptsächlich die Möglichkeit, falls der User möchte, auf
            // eine nicht beeinflussende Weise den Testmodus nochmals zu
            // benutzen. Der User wird dafür gefragt, falls er zustimmt, kann er
            // diesen benutzen; eine boolean wird gesetzt, die jeweilige
            // Dispatches [sic?] deaktiviert.
            randomize(learnQueue)
            setNextCard(learnQueue.shift())
            setQueue(learnQueue)
            setIsDoneLoading(true)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.cards])

    function doReset(){
        setIsDoneLoading(false)
        let cards = props.cards.cards;
        setDispatchMode(false)
        setIsDone(false)
        for(let i = 0; i < cards.length; i++){
            learnQueue.push(cards[i])
        }
        setNextCard(learnQueue.shift())
        setQueue(learnQueue)
        setIsDoneLoading(true)
    }

    function onEvent(){
        // Je nach gegebener Antwort wird die Phase und das 
        //nächste Datum gesetzt. Das Datum wird mithilfe des Levels bestimmt.
        if(isCorrect === true){
            let isWrong = false
            for(let i = 0; i < wrongQueue.length; i++){
                if(wrongQueue[i] === currentCard.rowIndex){
                    isWrong = true
                    break;
                }
            }
            if(currentCard.testPhase !== testPhase.ph5 && !isWrong && dispatchMode){
                props.incrementPhase(currentCard.rowIndex, currentCard.testPhase+1, props.id)
            }
            let nextDate = dateSelect(currentCard.testPhase)
            if(dispatchMode){
                props.setNextDate(currentCard.rowIndex, nextDate, props.id)
            }
        }
        // Falls die Abfrage der Karte gescheitert ist, wird die Karte wieder 
        // in die queue appended, bis sie richtig abgerufen wurde.
        if(!isCorrect){
            // Falls die Karte bereits falsch beantwortet und gesetzt wurde, soll sie ja nicht noch einmal in die Queue hinzugefügt werden.

            answeredWrong.push(currentCard.rowIndex)
            learnQueue.push(currentCard)
            setQueue(learnQueue)
        }
        // Das Set wird wieder unsichtbar gesetzt und die nächste Karte wird ausgewählt.
        setIsVisible(false)
        answer.value = ''
        selectCard()
    }

    function randomize(array) {
        var j, x, i;
        for (i = array.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = array[i];
            array[i] = array[j];
            array[j] = x;
        }
        return array;
    }

    function startPreperation(){
        let cards = props.cards.cards;
        let statValues = [
            { name: 'Phase 1', cards: 0 },
            { name: 'Phase 2', cards: 0 },
            { name: 'Phase 3', cards: 0 },
            { name: 'Phase 4', cards: 0 },
            { name: 'Phase 5', cards: 0 },
          ]
        let final = [];
        for(let i = 0; i < cards.length; i++){
            switch(cards[i].testPhase){
                case testPhase.ph1:
                    statValues[0].cards++
                    break;
                case testPhase.ph2:
                    statValues[1].cards++
                    break;
                case testPhase.ph3:
                    statValues[2].cards++
                    break;
                case testPhase.ph4:
                    statValues[3].cards++
                    break;
                case testPhase.ph5:
                    statValues[4].cards++
                    break;
                default:
                    break;
            }
        } 
        // eslint-disable-next-line array-callback-return
        statValues.map(function(obj){
            if(obj.cards !== 0){
                final.push(obj)
            }
        });
        return final
    }

    function selectCard(){
        // Hier wird die nächste anzuzeigende Karte zurückgegeben.
        let next = learnQueue.shift()
        if(next === undefined){
            setIsDone(true)
        } else {
            setNextCard(next)
        }
    }

    function submit(question, answer){
        if(compare(question, answer)){
            setResult(true) 
        } else {
            setResult(false)
        }
        setIsVisible(true)
    }

    if(props.cards === undefined) {
        return(
            <NotFound />
        )
    }
    else {
            return(
                !isDoneLoading ?
                <div className='component-view'>
                    Lade Deck ...
                    <div className="cssload-tetrominos">
                        <div className="cssload-tetromino cssload-box1"></div>
                        <div className="cssload-tetromino cssload-box2"></div>
                        <div className="cssload-tetromino cssload-box3"></div>
                        <div className="cssload-tetromino cssload-box4"></div>
                    </div>
                </div> :
                isDone || currentCard === undefined ?
                    <div className="component-view">
                        <h1>Gut gemacht!</h1>
                        <i>Fertig für heute! Komm die nächsten Tage wieder zurück, um die nächsten Karten zu spielen.</i><br/>
                        <Link to={`/deck/${props.id}`}><button>Zurück zur Übersicht</button></Link>
                        <p>Verteilung der Karten in diesem Deck:</p>
                        <LearnStatistics dataValues={startPreperation()}/>
                        <button onClick={doReset}>Trotzdem lernen</button>
                        <p><b>Hinweis:</b> Wenn du nochmal den Schreibmodus benutzen möchtest, haben die Ergebnisse keine Auswirkung auf die Karten, d. h. sie werden nicht in eine andere Phase verschoben.</p>
                    </div>
                :
                <div>
                <div className="component-view">
                    <i>Aktuelle Karte: Phase {currentCard.testPhase}</i>
                    <div className="test-card"> 
                         <div className="card-top">
                            <p>{currentCard.word}</p>
                        </div>
                    </div>
                    
                    <form onSubmit={e => { 
                        e.preventDefault()
                        submit(currentCard.definition, answer.value)
                        }
                    }>
                        { isVisible ?
                                <div className='input-container'>
                                    <input id="test-input" placeholder="Antwort" disabled type="text" ref={node => (answer = node)}/>
                                    <button disabled type="submit">Prüfen</button>
                                </div>
                            :
                                <div className='input-container'>
                                   {/*<input onKeyPress={(e)=>{e.target.keyCode === 13 && e.preventDefault();}} />*/}
                                    <input id="test-input" placeholder="Antwort" autoComplete='off' type="text" ref={node => (answer = node)}/>
                                    <button type="submit">Prüfen</button>
                                </div>
                        }
                    </form>
                    
                    </div>
                    {isVisible ? 
                        isCorrect ?
                            <div className="result-dialogue correct fadeInUp">
                                <h3>Prima!</h3>
                                <span>Antwort: <b>{currentCard.definition}</b></span>
                                <button type="submit" onClick={onEvent} id='continue-button' >Weiter</button>
                            </div>
                            :
                            <div className="result-dialogue wrong fadeInUp">
                                <h3>Falsch</h3>
                                <span>Richtige Antwort: <b>{currentCard.definition}</b></span>
                                <button type="submit" onClick={onEvent} id='continue-button'>Weiter</button>
                            </div>
                    : ''    
                    }
                </div>
                )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionLearn);