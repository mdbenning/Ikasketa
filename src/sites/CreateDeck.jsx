import React from 'react'
import { connect } from 'react-redux'
import TableInput from '../components/TableInput'
import { addDeck } from '../actions'
import { addCards } from '../actions'
import history from '../utils/history';
import idGen from '../utils/idgen'
import testPhase from '../utils/testPhase';

import '../style/deck.css'

const mapDispatchToProps = (dispatch) => {
    return {
        addDeck: (title, description, id) => {dispatch(addDeck(title, description, id))},
        addCards: (cards, id) => {dispatch(addCards(cards, id))}
    }
}
    
const CreateDeck = (props) => {
    let title, description 
    const sharedID = idGen()
    const deckCreation = (propsToDispatch) => {
        if(!title.value.trim()){
            // Falls DAU keinen Namen angibt, hier ein Fallback
            props.addDeck('Unbenanntes Deck', description.value, sharedID)
            props.addCards(propsToDispatch, sharedID)
            // Die Werte des Formulars (/create) werden hier zurückgesetzt
            title.value = ''
            description.value = ''
            history.push('/');
        } else {
            // Dispatch: Kartendeck wird erstellt.
            props.addDeck(title.value, description.value, sharedID)
            props.addCards(propsToDispatch, sharedID)
            title.value = ''
            description.value = ''
            history.push('/');
        }
    }
    
    return(
        <div className="component-view">
            <form onSubmit={e => { 
                e.preventDefault()
                }
            }>
                <input className="deck-name" placeholder="Gib einen Titel ein..." ref={node => (title = node)} />
                <input className="deck-desc" placeholder="Beschreibung hinzufügen" ref={node => (description = node)}/>            
            <br />
            <h2>Karten</h2>
           </form>
           <div className="tableWrap">
                <TableInput
                    initialCards = {{cards: [ {word: '', definition: '', rowIndex: 0, testPhase: testPhase.ph1, nextDate: 0}, {word: '', definition: '', rowIndex: 1, testPhase: testPhase.ph1, nextDate: 0}] }}
                    onSubmit={deckCreation}
                    createDeck={true}
                />
           </div>
        </div>
    )
}

export default connect(null, mapDispatchToProps)(CreateDeck)
