import React from 'react';
import auth from '../utils/auth'
import history from '../utils/history';


const LandingPage = () => {
    return (
        <div className="component-view">
            <h1>Landing blabla</h1>
            <button onClick={() => ( auth.login(() => history.push('/app') ))}>Anmelden</button>
        </div>
    )
}

export default LandingPage;