import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
//import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { removeDeck, removeCards, editDeck, editCards } from '../actions'
import 'react-confirm-alert/src/react-confirm-alert.css'
import { confirmAlert } from 'react-confirm-alert'
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NotFound from '../components/NotFound'
import TableInput from '../components/TableInput'

import history from '../utils/history';

import '../style/deck.css'

const mapStateToProps = (state, ownProps) => {
    const { id } = ownProps.match.params
    const deck = state.decks.find(deck => deck.id === id);
    const card = state.cideck.find(card => card.id === id)
    console.log(card)
    return {deck, card, id};
}

const mapDispatchToProps = (dispatch) => {
    return{
        removeDeck: (id) => dispatch(removeDeck(id)),
        removeCards: (id) => dispatch(removeCards(id)),
        editDeck: (title, description, id) => dispatch(editDeck(title, description, id)),
        editCards: (cardVal, id) => dispatch(editCards(cardVal, id))
    }
}

const DeckView = (props) => {
    let title, desc
    const [isEditable, setEditable] = useState(false);

    useEffect(() => {
        setEditable(false);
    }, [])

    const actionDelete = () => {
        props.removeDeck(props.deck.id);
        props.removeCards(props.card.id);
        history.push('/');
    }

    const actionEdit = (propsToDispatch) => {
        if (title.value === "") {
            props.editDeck("Unbenanntes Deck", desc.value, props.id)
            props.editCards(propsToDispatch, props.id)
        } else {
            props.editDeck(title.value, desc.value, props.id)
            props.editCards(propsToDispatch, props.id)
        }

        setEditable(false);
    }

    const deleteConfirm = () => {
        confirmAlert({
            title: 'Deck löschen',
            message: 'Möchtest du dieses Deck wirklich löschen?',
            buttons: [
                {
                    label: 'Ja!',
                    onClick: () => actionDelete()
                },
                {
                    label: 'Nein!',
                    onClick: () => null
                }
            ]
        })
    }
    
  if(!props.deck){
    return(
        <NotFound />
    )
} else {
    return(
            <div className="component-view">
                <Link to='/'><button>Zurück</button></Link>
                { isEditable ?
                <form
                onSubmit={(e) => {
                    e.preventDefault()
                }
                }>
                    <input className="deck-name edit-mode" type="text" defaultValue={props.deck.name} ref={node => (title = node)} />
                    <input className="deck-desc edit-mode" type="text" defaultValue={props.deck.description} placeholder="Beschreibung hinzufügen" ref={node => (desc = node)}/>
                </form>
                :   <div>
                        <h2 id="deck-title">{props.deck.name}<FontAwesomeIcon id="d-edit" onClick={setEditable.bind(true)} icon={faPen}/></h2>
                        <h3 id="deck-desc">{props.deck.description}</h3>
                        <h3><i>{props.card.cards.length} Karten</i></h3>
                    </div>
                 }
                 {isEditable ? 
                 <div>
                    <button disabled>Lernen</button>
                    <button disabled>Test</button>
                 </div> :
                 <div id='mode-selection'>
                    <Link to={`/deck/${props.deck.id}/learn`}><button>Lernen</button></Link>
                    <Link to={`/deck/${props.deck.id}/test`}><button>Test</button></Link>
                 </div>}
                <div className="deck-table">
                    <div className="table-row table-header">
                        <div className="view title-word">Wort</div>
                        <div className="view title-definition">Definition</div>
                    </div>

                    {isEditable ?
                        <TableInput initialCards={props.card} createDeck={false} onSubmit={actionEdit}/>
                    : ''}
                    {props.card.cards.map((card, rowId) => 
                    isEditable ?
                        ''
                    :
                    <div key={rowId+1} className="table-row">
                        <div className="view word">{card.word}</div>
                        <div className="view definition">{card.definition}</div>
                    </div>)}
                </div>
                {isEditable ? 
                '' :
                <button id="delete-button" onClick={deleteConfirm}>Deck löschen</button>
            }
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeckView);
