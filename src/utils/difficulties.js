const learnDif = Object.freeze({"unseen": 0, "pass": 1, "again": 2})

export default learnDif;