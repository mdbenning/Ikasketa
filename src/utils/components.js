const comp = Object.freeze({"learn": 0, "write": 1, "test": 2})

export default comp;