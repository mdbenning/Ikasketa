import testPhase from './testPhase'

var date = new Date();

function dateSelect(pTestPhase){
    switch(pTestPhase){
        // Die angegebenen Zahlen entsprechen n Tage in der Zukunft, in der
        // die jeweilige Karte wieder aufgerufen werden soll.
        case testPhase.ph1:
            return date.setDate(date.getDate() + 1)
        case testPhase.ph2:
            return date.setDate(date.getDate() + 3)
        case testPhase.ph3:
            return date.setDate(date.getDate() + 7)
        case testPhase.ph4:
            return date.setDate(date.getDate() + 14)
        case testPhase.ph5:
            return date.setDate(date.getDate() + 20)
        default:
            console.log('oops!')
    }
}
export default dateSelect;