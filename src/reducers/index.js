import {combineReducers} from 'redux'
import decks from './decks'
import cideck from './cideck'
import user from './user'

export default combineReducers({
    decks, cideck, user
})
