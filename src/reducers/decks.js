const initialState = [{
      "id": "3d1f801d-bb61-4faf-bde2-0b4b1c7dc825",
      "name": "Französisch I",
      "description": "Einführung in die französische Sprache"
    }, 
    { 
        "id": "f2d3e0f8-8fcf-4611-ba7d-222c9b7990af",
        "name": "Englisch 1",
        "description": "Paar nette Wörter"
    }
]


const decks = (state = initialState, action) => {
    switch(action.type) {
        case 'ADD_DECK':
            return[
                ...state, {
                    id: action.id,
                    //arrayID: action.id,
                    name: action.name,
                    description: action.description
                }
            ]
        case 'EDIT_DECK':
            let edited_state = state.find(deck => deck.id === action.id)
            edited_state = {...edited_state,
                    name: action.name,
                    description: action.description,
                }
            let deckList = state
            for(let i = 0; i < state.length; i++){
                if(state[i].id === action.id){
                    deckList.splice(i, 1, edited_state);
                    return deckList;
                }
            }
            return state
        case 'REMOVE_DECK':
            const result = state.filter(deck => deck.id !== action.id)
            return result
        default:
            return state   
    }
}

export default decks;
