const initialState = [
        {"cards": [ 
          { "word": "Bienvenue !", "definition": "Willkommen!", "rowIndex": 0, "testPhase": 1, "nextDate": 0 },
          { "word": "Je m'appelle...", "definition": "Mein Name ist...", "rowIndex": 1, "testPhase": 1, "nextDate": 0 }, 
          { "word": "Et vous ?", "definition": "Und Sie?", "rowIndex": 2, "testPhase": 1, "nextDate": 0 },
          { "word": "Bonsoir !", "definition": "Guten Abend!", "rowIndex": 3, "testPhase": 1, "nextDate": 0 },
          { "word": "Enchanté", "definition": "Erfreut", "rowIndex": 4, "testPhase": 1, "nextDate": 0 },
          { "word": "Comment allez-vous ?", "definition": "Wie geht es Ihnen?", "rowIndex": 5, "testPhase": 1, "nextDate": 0 },
          { "word": "Ça va ?", "definition": "Wie gehts?", "rowIndex": 6, "testPhase": 1, "nextDate": 0 },
          { "word": "trés bien", "definition": "sehr gut", "rowIndex": 7, "testPhase": 1, "nextDate": 0 },
          { "word": "À plus!", "definition": "Bis bald!", "rowIndex": 8, "testPhase": 1, "nextDate": 0 },
          { "word": "pardon", "definition": "Entschuldigung", "rowIndex": 9, "testPhase": 1, "nextDate": 0 },
          { "word": "la haine", "definition": "der Hass", "rowIndex": 10, "testPhase": 1, "nextDate": 0 },
          { "word": "embruns", "definition": "Gischt", "rowIndex": 11, "testPhase": 1, "nextDate": 0 }  
        ],
        "id": "3d1f801d-bb61-4faf-bde2-0b4b1c7dc825"
        },
        {"cards": [
          { "word": "Guten Morgen !", "definition": "Good morning!", "rowIndex": 0, "testPhase": 1, "nextDate": 0 },
          { "word": "Ich mag Kuchen", "definition": "I like cake", "rowIndex": 1, "testPhase": 1, "nextDate": 0 }, 
          { "word": "Mein Name ist...", "definition": "My name is...", "rowIndex": 2, "testPhase": 1, "nextDate": 0 },
          { "word": "Bis später!", "definition": "See you later!", "rowIndex": 3, "testPhase": 1, "nextDate": 0 },
          { "word": "Prost!", "definition": "Cheers!", "rowIndex": 4, "testPhase": 1, "nextDate": 0 },
          { "word": "Wie alt bist du?", "definition": "How old are you?", "rowIndex": 5, "testPhase": 1, "nextDate": 0 },
          { "word": "Geringschätzung", "definition": "floccinaucinihilipillification", "rowIndex": 6, "testPhase": 1, "nextDate": 0 },
          { "word": "Wunder", "definition": "miracle", "rowIndex": 7, "testPhase": 1, "nextDate": 0 },
          { "word": "Programmieren", "definition": "to program", "rowIndex": 8, "testPhase": 1, "nextDate": 0 },
        ],
        "id": "f2d3e0f8-8fcf-4611-ba7d-222c9b7990af"
        },
]

const cards = (state = initialState, action) => {
    let edited_state, selectedCard, updatedState, updatedCard, ind
    switch(action.type) {
        case 'ADD_CARDS':
            ind = 0
            let newCards = action.cards
            newCards.map(card => (card.rowIndex = ind++))
            return [...state, {
                cards: newCards,
                id: action.id,
            }
        ]
        case 'EDIT_CARDS':
            ind = 0
            let updatedCards = action.cards
            updatedCards.map(card => (card.rowIndex = ind++))
            let updatedList = state;
            let index
            updatedCards = {"cards": updatedCards, "id": action.id}
            for(let i = 0; i < updatedList.length; i++){
                if(updatedList[i].id === action.id){
                    index = i
                }
            }
            updatedList.splice(index, 1, updatedCards);
            return updatedList
        case 'REMOVE_CARDS':
            const result = state.filter(card => card.id !== action.id)
            return result
        case 'PHASE_INCREMENT':
            edited_state = state.find(card => card.id === action.id)
            selectedCard = edited_state.cards.find(card => card.rowIndex === action.rowIndex)
            updatedState = state
            updatedCard = {...selectedCard, testPhase: action.phase}
            for(let i = 0; i < edited_state.cards.length; i++){
                
                if(edited_state.cards[i].rowIndex === action.rowIndex){
                    edited_state.cards.splice(i, 1, updatedCard)
                    for(let k = 0; updatedState.length; k++){
                        if(updatedState[k].id === action.id){
                            updatedState.splice(k, 1, edited_state)
                            return updatedState
                        }
                    }
                }
            }
            return state;
        case 'PHASE_DECREMENT':
            // Könnte irgendwann in der Zukunft implementiert werden, war jedoch im Rahmen der gewollten Funktionen noch nicht notwendig.
            return state
        case 'SET_NEXT_DATE':
            edited_state = state.find(card => card.id === action.id)
            selectedCard = edited_state.cards.find(card => card.rowIndex === action.rowIndex)
            updatedState = state
            updatedCard = {...selectedCard, nextDate: action.date}
            for(let i = 0; i < edited_state.cards.length; i++){
                if(edited_state.cards[i].rowIndex === action.rowIndex){
                    edited_state.cards.splice(i, 1, updatedCard)
                    for(let k = 0; updatedState.length; k++){
                        if(updatedState[k].id === action.id){
                            updatedState.splice(k, 1, edited_state)
                            return updatedState
                        }
                    }
                }
            }
            return state;
        case 'RECEIVE_CARD':
            return state
        default:
            return state;
    }
}

export default cards;