import React from 'react';
import './style/main.css'
import Nav from './sites/Nav';
import Help from './sites/Help';
import {Router, Switch, Route} from 'react-router-dom';
//import {ProtectedRoute} from './components/ProtectedRoute'

//import LandingPage from './sites/LandingPage'
import Main from './sites/Main'
import CreateDeck from './sites/CreateDeck';
import DeckView from './sites/DeckView';
import ActionLearn from './sites/ActionLearn'
import history from './utils/history'
import ActionTest from './sites/ActionTest';
import NotFound from './components/NotFound';
//import auth from './utils/auth';

function App() {
  return (
    <div>
      <Router history={history}>
        <Nav />
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/help" component={Help}/>
          <Route path="/create" component={CreateDeck}/>
          <Route path="/deck/:id" exact component={DeckView}/>
          <Route path="/deck/:id/learn" exact component={ActionLearn}/>
          <Route path="/deck/:id/test" exact component={ActionTest}/>
          <Route component={NotFound}/>
        </Switch>
      </Router>
    </div>
    );
}

export default App;
