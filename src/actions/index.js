// cideck = cards in deck
export const ADD_CARD = 'ADD_CARD'
export const DELETE_CARD = 'DELETE_CARD'

export const addDeck = (name, description, id) => ({
    type: 'ADD_DECK',
    id,
    name,
    description
})

export const removeDeck = id => async dispatch => {
    dispatch({
        type: 'REMOVE_DECK',
        id
    })   
}

export const addCards = (cards, id) => ({
    type: 'ADD_CARDS',
    cards,
    id,
})

export const editCards = (cards, id) => ({
    type: 'EDIT_CARDS',
    cards,
    id
})

export const removeCards = (id) => async dispatch => {
    dispatch({
        type: 'REMOVE_CARDS',
        id
    })
}

// Keine Funktionalität
export const decrementPhase = (rowIndex, phase, id) => async dispatch => {
    dispatch({
        type: 'PHASE_DECREMENT',
        rowIndex,
        phase,
        id
    })
}

export const incrementPhase = (rowIndex, phase, id) => async dispatch => {
    dispatch({
        type: 'PHASE_INCREMENT',
        rowIndex,
        phase,
        id
    })
}

export const setNextDate = (rowIndex, date, id) => async dispatch => {
    dispatch({
        type: 'SET_NEXT_DATE',
        rowIndex,
        date,
        id
    })
}

export const editDeck = (name, description, id) => ({
    type: 'EDIT_DECK',
    name,
    description,
    id
})

export const ShowDecks = {
    SHOW_ALL: 'SHOW_ALL'
}
