# Ikasketa

Ikasketa is a free online/offline vocabulary trainer without premium options and excessive gimmicks, programmed in React. It gives you the opportunity to concentrate on what is important: learning.  And learning is a commodity that should benefit everyone.

Ikasketa gives you different ways to learn. You can create your own personal vocabulary cards, for example to improve your language skills, but also to learn terms for other topics. You can also search for decks from other users and add them to your collection.

**Ikasketa is and will always be free software.**

## Installation

Ikasketa can be used online and offline alike. If you wish to run it on your computer, please run it as following:
```
npm install
npm start
```

## Instances
The example instance was shut down in May 2020.
