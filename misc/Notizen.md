# Ikasketa

## Gestaltungsbeobachtungen
### Beispiel Quizlet
Quizlet bietet verschiedene Decks an, in denen die Karten allgemein eingetragen werden können.

In diesen Decks selbst können verschiedene Arten von Tests benutzt werden, um Inhalte zu erlernen. Darunter lassen sich z. B. *Testsektionen*, Schreiben, *Matching* als auch ein sogenannter "Lernmodus" finden.

## Rückschlüsse
Für die linerare Entwicklung wäre folgende Struktur anzugehen:
- übergeordnete Menüs zur Ansicht von Decks und deren Verwaltung (Löschen, Erstellen, Ordnen)
- Deckfunktionen selbst
    - Menüansicht
    - Erstellen und eintragen von Karten
    - Verwalten in einer Datenbank (Input/Output mithilfe von Redux)
- Kartenlernen
    - Daten aus einer Liste übernehmen, einzeln printen, und andere Inhalte sichtbar machen
    - Auf den Input des Users reagieren und die Karte einsortieren (Schwer/Geht/Einfach/Sehr einfach)
    - Je nach Umstand wiederholen (wie Quizlets "Lernfunktion")
    - Übertragen der Daten in eine Statistik (wieviele oft falsch/richtig, allgemeiner Lernfortschritt)
- "Schreiben"
    - Daten aus einer Liste übernehmen, printen, und ein Textfeld für eine Lösung anbieten
    - Übergebene Daten aus dem Textfeld überprüfen und das Feedback "falsch" oder "richtig" nach Vergleich mit der Lösung zurückgeben.
        - Dabei auf Variationen achten (Lowercase/uppercase, Buchstabe versehentlich vertauscht aber richtige Intention, ab bestimmter Menge von Fehlern jedoch als falsch angeben usw.)
- Test
    - Art *optionaler Modus*
    - Gemischte Funktionen
        - Nicht nur von der Eingangs- zur Ausgangssprache, sondern auch getauscht
        - Aufgaben wie "aus Lösung auswählen" (hier Daten zufällig aus anderen Karten fetchen und als Lösungsfeld anbieten)
        - Teilweise auch Textfelder zum Einsetzen von Lösungen
            - optional auch mit genauester Kontrolle (orthographisch korrekt)
    - Auf Zeit
- Einzelne Tweaks (in Funktionen, nette Gimmicks)
- Sprachenfunktionen

## Beobachtungen allgemein
- Die Stateverwaltung war (bis jetzt) eine Schwierigkeit und hat eine ziemliche Schwierigkeit bedeutet.
- Die Darstellung in DeckView ist eine Herausforderung, da das darstellen in tables viele Bugs mit sich bringt – des Weiteren ist es ja das Ziel, einzelne Elemente per Doppelklick bearbeiten zu können.
    - Flex war/ist die Lösung, jedoch ist die Hierachie natürlich verwirrend - einerseits durch die Mapfunktion -> Korrektes Wrappen der Container.
- Dispatch für das Löschen für Decks
- Implementierung von SM2 https://www.supermemo.com/en/archives1990-2015/english/ol/sm2
    - I(n) = I(n-1) * E
    - I = Intervals in days
    - n = Number of repitions
    - E = ???
- ODER nach phase6
- https://help.quizlet.com/hc/en-us/articles/360030429692
- Problematiken, den Lernmechanismus einzubauen -> Implementierung von einer Queue für "Pass"/"Again"
- Allgemeine Datenstruktur hatte keine... Struktur?

## Weitere Ressourcen
Illustrationen: ASCII, weil lustig.
- https://www.asciiart.eu/books/books
- Spinner: https://icons8.com/cssload/en/3d-loaders

## Phase 6 - Implementation
- Phase 6 ist für die Implementation im Projekt geeignet.
- Angaben im Reducer: 
  - Aktuelle Phase (Standardmäßig 2, da phase6 die Karten
  erstmals revidieren möchte -> Schlussendlich 5 Phasen)
  - Nächste Abfrage

## Abfrage im Testmodus
- Reguläre Ausdrücke sollen überprüfen, "wie richtig" das Ergebnis ist. Einfache
  Tippfehler sollen noch akzeptiert werden.
